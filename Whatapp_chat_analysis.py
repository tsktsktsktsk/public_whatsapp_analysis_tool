# -*- coding: utf-8 -*-
"""
Created on Sat Feb 25 11:28:37 2017

@author: ik-be
"""

import operator
import re
from time import mktime
import datetime
from matplotlib import pyplot as plt

class Wapp_histogram(object):
    
    """Returns two lists with histograms: [0] = sender, [1] = receiver.
    The directory argument is the complete path to the .txt whatsapp conversation file.
    The sender argument is the full name of the sender (probably your name in whatsapp).
    The receiver argument is the full name of the receiver."""
    
    def __init__(self, directory):
        self.directory = directory

    def opener(self):
        file_object = open(self.directory, 'r')
        text = file_object.read()
        return text
        
        
    def fix_date_format(self, date_string): #Quick dirty fix
        #This function is written feb 2017 to change the date format back to what it was a year ago ('xx/xx/xxxx')
        _, month, day, year, _ = re.split('([\d]*)[/]([\d]*)[/]([\d]*)', date_string)
        month = month if len(month) == 2 else '0' + month
        day = day if len(day) == 2 else '0' + day
        year = '20' + year
        return '{}/{}/{}'.format(day, month, year)
            
    def remove_usernames(self):
        strings = ["messages",
                   'changed',
                   "removed",
                   "added"]
        new_username_list = []          
        for username in self.usernames:
            found = False
            for string in strings:
                if string in username.lower():
                    found = True
            if found == False:
                new_username_list.append(username)

        self.usernames = new_username_list
                    
    def text_splitter(self):
        text = self.opener()
        """[1] = date, [3] = time, a[5] = username, a[7] = message
        [8] = date, a[10] = time, a[12] = username, a[14] = message
        Returns:
            [0] = a list with all the dates something has been said.
            [1] = a list of times something has been said.
            [2] = a list of usernames
            [3] = a list of messages."""
        print text[:100]
        text_list = re.split('([\d]*[/][\d]*[/][\d]*)([,][\s])([\d]*[:][\d]*)[\s][-][\s]([a-zA-Z]*[\s]*[^:]*)(:)([a-zA-Z\s]*)', text)
        print text_list[:12]
        date_list, time_list, username_list, message_list = [], [], [], []
        self.date_list, self.time_list, self.username_list, self.message_list = date_list, time_list, username_list, message_list
        for i in range(0, len(text_list)-7, 7):
            date_list.append(self.fix_date_format(text_list[i+1]))
            time_list.append(text_list[i+3])
            username_list.append(text_list[i+4])
            message_list.append(text_list[i+6])
        return self.date_list, self.time_list, self.username_list, self.message_list
      
    def username_finder(self):
        """Returns a list of:
        [0] = a dictionary with the username as key 
        and a list of messages as value.
        [1] = a list of usernames
        [2] = a dictionary with the username as key 
        and as value the indexis of the messages per user."""
        total_list = self.text_splitter()
        self.usernames = list(set(total_list[2]))
        self.remove_usernames() #Added feb 2017 to remove the end to end encryption string from the 'usernames'
        self.username_indexis = {} #gives the index of what user said what
        self.user_text_dict = {}

        for user in self.usernames:
            self.username_indexis[user] = []
            self.user_text_dict[user] = []

        for i in range(len(total_list[2])):
            for user in self.usernames:
                if total_list[2][i] == user:     
                    self.user_text_dict[user].append(total_list[3][i])
                    self.username_indexis[user].append(i)
        
        for user in self.usernames:
            print user, ' has the following amount of messages: ', len(self.user_text_dict[user]) 
        return self.user_text_dict, self.usernames, self.username_indexis
                
    def word_isolator(self, string):
        """Returns a list of words from the string."""
        woorden, spatie_index = [], [0]
        string = string.lower().strip()
        for i in range(len(string)):
            if string[i:i+1] == ' ':
                spatie_index.append(i+1)
        if len(spatie_index) > 1:
            for i in range(len(spatie_index)-1):
                
                woorden.append(string[spatie_index[i]:spatie_index[i+1]].strip().lower())
            return woorden
        else:
            return [string.lower().strip()]
    
    def word_dict_builder(self):
        """Returns a dictionary with the users as 
        key and a list of messages as value."""
        user_dict = self.username_finder()[0]
        total_user_word_dict = {}
        word_length_dict = {}
        for user in self.usernames:
            teller = 0
            word_length_dict[user] = []
            total_user_word_dict[user] = []
            for message in user_dict[user]:
                message_word_list = self.word_isolator(message.lower().strip())
                for word in message_word_list:

                    total_user_word_dict[user].append(word)
                    if len(word.strip()) > 0:
                        teller += 1.0
                        word_length_dict[user].append(len(word))
            
            word_length_dict[user] = sum(word_length_dict[user]) / teller
            print user, ' has an average word length of: ', round(word_length_dict[user], 4)
            
        return total_user_word_dict
 
    def word_dict_hist(self):
        """Dict keys are the usernames and the values are 
        another dict wherein the keys are the words and the
        values are the frequency.
        [0] = a dictionary with the users as key --> a dictionary with the unique words as key and values the frequency (histogram)
        [1] = a list of usernames"""
        
        self.hist_dict = {}
        user_word_dict = self.word_dict_builder()
        for user in self.usernames:
            self.hist_dict[user] = {}
            for word in user_word_dict[user]:
                try:
                    if self.hist_dict[user][word] != 0:
                        self.hist_dict[user][word] += 1
                except KeyError:
                    self.hist_dict[user][word] = 1
            self.hist_dict[user] = sorted(self.hist_dict[user].items(), key=operator.itemgetter(1), reverse = True) 
            print user, ' has said ', len(self.hist_dict[user]), ' unique words.'
        return self.hist_dict, self.usernames  
    
    def new_words_per_day(self):
        """This can be an excellent extension 
        on the histogram builder without the 
        need of being a unique function.
        It is ubiquitous to reshape the whole sentences."""
        self.new_words_day_dict = {}
        usernames = self.usernames
        freq = self.date_msg_count_dict
        new_words_day_dict = {}
        new_word_dict = {}
        messages = self.user_text_dict
        for user in usernames:
            teller = 0
            new_words_day_dict[user] = []
            new_word_dict[user] = {}
            for date_freq in freq[user]:
                new_words_day_dict[user].append([date_freq[0], 0])
                for i in range(date_freq[1]):
                    k = teller + i-1
                    message = self.word_isolator(messages[user][k])
                    
                    for word in message:
                        
                        try:
                            if new_word_dict[user][word] != 0:
                                new_word_dict[user][word] += 1
                        except KeyError:
                            new_word_dict[user][word] = 1
                            new_words_day_dict[user][-1][1] += 1
                    
                
                teller += date_freq[1]
            self.new_words_day_dict[user] = new_words_day_dict[user]
            self.new_words_day_dict[user] = self.datefix(user, False)
            lijst = list(self.new_words_day_dict[user])
            new_words_day_dict[user] = lijst
        return new_words_day_dict
                
                
                
        
    
    def msg_on_hour_counter(self):
        """Gives the frequency graph of the amount of messages on the specific hour of day.
        [0] = a dict with the minutes of the clock as key (00:00 > 23:59) with the amount of messages on that time.
        [1] = only the values
        [2] = a list with all the hours of the clock (functional as a list of keys for the dict)"""
        
        usernames = self.username_indexis
        hour_msg_count_dict = {}
        time_line = {}
        hours = {}

        for user in usernames:
            hour_msg_count_dict[user] = {}
            for i in range(24):
                for j in range(60):
                    hour_msg_count_dict[user][str(str(str(i) if i >= 10 else str('0' + str(i))) + ':' + str(str(j) if j >= 10 else str('0' + str(j))))] = 0
            for i in self.username_indexis[user]:
                time = self.time_list[i]
                hour_msg_count_dict[user][time] += 1

            hour_msg_count_dict[user] = sorted(hour_msg_count_dict[user].items(), key=operator.itemgetter(0), reverse = False)
            time_line[user] = [list(i)[1] for i in hour_msg_count_dict[user]]
            hours[user] = [list(i)[0] for i in hour_msg_count_dict[user]]
            
        return hour_msg_count_dict, time_line, hours
    
    def date_to_unix(self, date):
        """Just a helper function for converting the time format to unix time."""
        year = int(date[-4:])
        month = int(date[3:5])
        day = int(date[:2])      
        unix_date = mktime(datetime.datetime(year, month, day).timetuple())
        return unix_date        
     
    def msg_on_date_counter(self):
        """Gives the amount of messages per date.
        This function can be shortened quite some.
        returns a list of lists:
        [0] = a dict with the unix_dates as key with a counter of messages send on that date
        [1] = a list with only the values. """
        usernames = self.usernames
        self.date_msg_count_dict = {}
        time_line = {}
        for user in usernames:
            self.date_msg_count_dict[user] = {}
            for i in self.username_indexis[user]:
                unix_date = self.date_to_unix(self.date_list[i])

                try:
                    if self.date_msg_count_dict[user][unix_date] > 0:
                        self.date_msg_count_dict[user][unix_date] += 1
                except KeyError:
                    self.date_msg_count_dict[user][unix_date] = 1
            self.date_msg_count_dict[user] = sorted(self.date_msg_count_dict[user].items(), key=operator.itemgetter(0), reverse = False) 
                
            min_date = min(self.date_msg_count_dict[user], key=lambda date: date[1])
            max_date = max(self.date_msg_count_dict[user], key=lambda date: date[1])
            min_unix_date = min(self.date_msg_count_dict[user], key=lambda date: date[0])[0]
            non_unix_min_date = datetime.datetime.fromtimestamp(int(min_date[0])).strftime('%Y-%m-%d')
            non_unix_max_date = datetime.datetime.fromtimestamp(int(max_date[0])).strftime('%Y-%m-%d')

            print "Least messages send on: ", non_unix_min_date, " for user ", user, " with ", min_date[1]
            print "Most messages send on: ", non_unix_max_date, " for user ", user, " with ", max_date[1]
            
            ##Fill in the empty dates.
            self.min_unix_date = min_unix_date
            time_line[user] = self.datefix(user, True)
        return self.date_msg_count_dict, time_line
    
    def datefix(self,user, boolean):
        #Extension of the above code.
        
        if boolean == True:
            date_count = self.date_msg_count_dict[user] 
        else:
            date_count = self.new_words_day_dict[user]
        time_list, time_list2 = [], []
        counter = 0
        #Create a list with day counter starting from the users first datum.
        #Dates with 0 messages are added in between.
        #I am quite sure this can be done much more succinctly.
        for date in date_count :
            
            time_list.append([int(date[0] - self.min_unix_date)/ 84600, date[1]])     
        for i in range(len(time_list)-1):
            difference = time_list[i+1][0] - time_list[i][0]
            counter += 1
            if difference > 1:
                counter += difference
                for j in range(time_list[i][0], time_list[i+1][0]):
                    time_list2.append([j, 0])
            else:
                time_list2.append(time_list[i])
        return [list(i) for i in time_list2]  
        
        
def clean_username(username):
    new_username = ''
    for char in username:
        if ord(char) < 128: #matplotlib labels don't work otherwise
            new_username += char
    return new_username
    
    
import time
start = time.time()
       
path = '.../WhatsApp Chat with Person.txt'

wh = Wapp_histogram(path)
hist, users = wh.word_dict_hist()

dates, tijd = wh.msg_on_date_counter()
hour, time_line, hours  = wh.msg_on_hour_counter()
daywords = wh.new_words_per_day()


for user in users:
    print user
    print hist[user][:50]


plt.figure(figsize = (16, 10))
ax = plt.subplot(111)
for user in users:
    plt.plot([list(i)[1] for i in hist[user]], label = clean_username(user))
    
ax.set_yscale('log')
ax.set_xscale('log')
ax.set_ylabel('Number of times occured (log)', fontsize=30)
ax.set_xlabel('Number of unique words (log)', fontsize=30)
plt.title('Distribution of word frequency', fontsize=30)
plt.legend() 
plt.show()

plt.figure(figsize = (16, 10))
ax = plt.subplot(111)
for user in users:
    plt.plot([list(i)[1] for i in hist[user]], label = clean_username(user))
    
ax.set_ylabel('Number of times occured', fontsize=30)
ax.set_xlabel('Number of unique words', fontsize=30)
plt.title('Distribution of word frequency', fontsize=30)
plt.legend() 
plt.show()

plt.figure(figsize = (16, 10))
ax = plt.subplot(111)
for user in users:

    plt.plot(time_line[user], label = clean_username(user))

ax.set_ylabel('Number of messages', fontsize=30)
ax.set_xlabel('Time of the day (hours)', fontsize=30)
plt.xticks(range(0, 1440, 60), range(0, 25))
plt.title('Distribution of messages at time of the day (per minute)', fontsize=30)
plt.legend()
plt.show()


plt.figure(figsize = (16, 10))
ax = plt.subplot(111)
for user in users:
    plt.plot([i[1] for i in tijd[user]], label = clean_username(user))
ax.set_ylabel('Number of messages', fontsize=30)
ax.set_xlabel('Days', fontsize=30)
plt.title('Distribution of messages per day', fontsize=30)
plt.legend()
plt.show()


plt.figure(figsize = (16, 10))
ax = plt.subplot(111)
for user in users:
    plt.plot([i[1] for i in daywords[user]], label = clean_username(user))

ax.set_ylabel('Amount of words added', fontsize=30)
ax.set_xlabel('Days', fontsize=30)
plt.title('New words added to the dictionary', fontsize=30)
plt.legend()
plt.show()


print "Completed in: ", time.time() - start, " seconds."
